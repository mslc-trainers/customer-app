package com.pwc.customerapp.servlets;

import com.pwc.customerapp.model.Customer;
import com.pwc.customerapp.services.CustomerService;
import com.pwc.customerapp.services.CustomerServiceFactory;
import com.pwc.customerapp.services.CustomerServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Request Processing
 * Must not implement database logic
 * <p>
 * SINGLE RESPONSIBILITY PRINCIPLE (Your class must change for 1 Reason)
 */
@WebServlet(name = "CustomerServlet", value = "/customer-servlet")
public class CustomerServlet extends HttpServlet {


    /**
     *
     * if this was the part of spring framework
     * @Autowired
     */
    CustomerService customerService = CustomerServiceFactory.createCustomerService();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        System.out.println("doGet is executed.....");
        boolean editMode = true;
        String id = request.getParameter("id");
        if (id == null || id.length() == 0) {
            editMode = false;
        }

        if (!editMode) {
            try {
                List<Customer> customers = customerService.getCustomers();
                request.setAttribute("customers", customers);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("customer-listing.jsp");
            dispatcher.forward(request, response);
        } else {


            try {
                int customerId = 0;
                try {
                    customerId = Integer.parseInt(id);
                    Customer customer = customerService.getCustomer(customerId);
                    request.setAttribute("customer", customer);
                } catch (NumberFormatException ex) {
                    request.setAttribute("error", "Invalid Id : " + id);
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("edit-customer.jsp");
            dispatcher.forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("doPost is executed........");
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        int customerId = 0;
        try {
            customerId = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            // handle the exception....
        }

        Customer customer = new Customer(customerId, name, address);

        try {
            customerService.saveCustomer(customer);
//            RequestDispatcher dispatcher = request.getRequestDispatcher("customer-servlet");
//            dispatcher.forward(request, response);
            response.sendRedirect("customer-servlet");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }


    }
}
