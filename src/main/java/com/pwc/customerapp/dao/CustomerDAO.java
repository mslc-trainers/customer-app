package com.pwc.customerapp.dao;

import com.pwc.customerapp.model.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * All database related logic must be implemented here
 */
public class CustomerDAO {


    public List<Customer> getCustomers() throws SQLException {

        Connection connection = DBConnectionHelper.getConnection();


        PreparedStatement stmt = connection.prepareStatement("Select id, name, address from Customer");
        ResultSet rs = stmt.executeQuery();
        List<Customer> customers = new ArrayList<>();
        while (rs.next()) {

            int id = rs.getInt("id");
            String name = rs.getString("name");
            String address = rs.getString("address");
            customers.add(new Customer(id, name, address));

        }
        return customers;
    }


    public Customer getCustomer(int id) throws SQLException {

        Connection connection = DBConnectionHelper.getConnection();

        PreparedStatement stmt =
                connection.prepareStatement("Select id, name, address from Customer where id = ?");
        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            int customerId = rs.getInt("id");
            String name = rs.getString("name");
            String address = rs.getString("address");
            Customer customer = new Customer(customerId, name, address);
            return customer;
        } else {
            throw new RuntimeException("Record with : " + id + " Not Found");
        }

    }

    public void saveCustomer(Customer customer) throws SQLException {

        Connection con = DBConnectionHelper.getConnection();

        String updateSQL = "Update Customer set name = ?, address = ? where id = ?";
        PreparedStatement stmt = con.prepareStatement(updateSQL);

        stmt.setString(1, customer.getName());
        stmt.setString(2, customer.getAddress());
        stmt.setInt(3, customer.getId());

        stmt.execute();


    }

}
