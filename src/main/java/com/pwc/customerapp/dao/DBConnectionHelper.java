package com.pwc.customerapp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionHelper {


    private static Connection con = null;

    public static Connection getConnection() throws SQLException {

        if (con != null) {
            return con;
        }

        con = DriverManager
                .getConnection("jdbc:mysql://192.168.29.83:3306/training",
                        "user", "user");


        return con;


    }

}
