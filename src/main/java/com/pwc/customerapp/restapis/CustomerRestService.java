package com.pwc.customerapp.restapis;


import com.pwc.customerapp.model.Customer;
import com.pwc.customerapp.services.CustomerServiceFactory;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;

@Path("/customers")
public class CustomerRestService {


    @GET
    public Response handleGetCustomers() throws SQLException {

        System.out.println("in handleGetCustomers...");
        List<Customer> customers = CustomerServiceFactory
                .createCustomerService()
                .getCustomers();

        return Response
                .ok()
                .entity(customers)
                .build();

    }

    @GET
    @Path("{id}")
    public Response handleGetCustomerById(@PathParam("id") String id) {

        return null;

    }

    @POST

    public Response handleCreateCustomer(@PathParam("id") String id) {

        return null;

    }


}
