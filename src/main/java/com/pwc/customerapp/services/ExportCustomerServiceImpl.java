package com.pwc.customerapp.services;

import com.pwc.customerapp.dao.CustomerDAO;
import com.pwc.customerapp.model.Customer;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class ExportCustomerServiceImpl implements CustomerService {

    CustomerDAO customerDAO = new CustomerDAO();

    public void saveCustomer(Customer customer) throws SQLException {


        if (customer == null) {
            throw new RuntimeException("Customer cannot be null");
        }

        if (customer.getName() == null || customer.getName().length() <= 5) {
            throw new RuntimeException("Name cannot be blank and it must be greater than 5");
        }

        // all other business specific validations

        /**
         *
         * Modified
         */
        customer.setModifiedDate(LocalDate.now());

        customerDAO.saveCustomer(customer);


    }

    public List<Customer> getCustomers() throws SQLException {

        return customerDAO.getCustomers();

    }

    public Customer getCustomer(int customerId) throws SQLException {

        return customerDAO.getCustomer(customerId);
    }
}
