package com.pwc.customerapp.services;

import com.pwc.customerapp.model.Customer;

import java.sql.SQLException;
import java.util.List;

public interface CustomerService {

    public List<Customer> getCustomers() throws SQLException;

    public Customer getCustomer(int id) throws SQLException;

    public void saveCustomer(Customer customer) throws SQLException;

}
