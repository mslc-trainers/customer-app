package com.pwc.customerapp.services;

public class CustomerServiceFactory {


    public static CustomerService createCustomerService() {

        return new CustomerServiceImpl();
    }
}
