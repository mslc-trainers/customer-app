<%@ page import="java.util.List" %>
<%@ page import="com.pwc.customerapp.model.Customer" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Customers</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
            crossorigin="anonymous"></script>
</head>
<body>


<%

    Customer customer = (Customer) request.getAttribute("customer");
    String error = "";
    if (customer == null) {
        error = (String) request.getAttribute("error");
    }

%>

<div class="container col-xxl-8 px-4 py-5">

    <% if (customer == null) { %>
    <div class="alert alert-error">
        <h2>
            <%=error%>
        </h2>
    </div>

    <% } else { %>
    <form action=""  method="post" >


        <div class="row">
            <div class="col-5">
                <div class="mb-3">
                    <label for="idInput" class="form-label">Id</label>
                    <input value="<%=customer.getId()%>" type="text" class="form-control" id="idInput" name="id"
                           placeholder="Enter Id">
                </div>
                <div class="mb-3">
                    <label for="nameInput" class="form-label">Name</label>
                    <input value="<%=customer.getName()%>" type="text" class="form-control" id="nameInput" name="name"
                           placeholder="Enter Name">
                </div>
                <div class="mb-3">
                    <label for="addressInput" class="form-label">Address</label>
                    <input value="<%=customer.getAddress()%>" type="text" class="form-control" id="addressInput"
                           name="address"
                           placeholder="Enter Address">
                </div>


                <button class="btn btn-primary" type="submit">Save</button>
            </div>
        </div>

    </form>
    <% } %>

</div>


</body>
</html>