<%@ page import="java.util.List" %>
<%@ page import="com.pwc.customerapp.model.Customer" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Customers</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
            crossorigin="anonymous"></script>
</head>
<body>

<%

    List<Customer> customers = (List<Customer>) request.getAttribute("customers");


%>

<div class="container col-xxl-8 px-4 py-5">

    <div class="lead">Customers</div>
    <hr>
    <% for (Customer c : customers) { %>
    <div class="row">
        <div class="col"><%=c.getId()%>
        </div>
        <div class="col"><%=c.getName()%>
        </div>
        <div class="col"><%=c.getAddress()%>
        </div>
        <div class="col">
            <a href="customer-servlet?id=<%=c.getId()%>">Edit</a>
        </div>
    </div>
    <% } %>


</div>
</body>
</html>